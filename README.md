# SAÉ 1.02 : 
Comparaison d'approches algorithmique !
=============================================
Collaborateurs :
- TUAILLON Léo
- CALATAYUD Yvan
- HERSAN Mathéo

Dans le cadre de la SAÉ 1.02 "Comparaison d'approches algorithmique !", nous avons conçu cette application en langage C qui a pour but de mettre en relation des étudiants avec les différentes formations d'iut en France pour que ceux-ci puissent ajouter leur candidatures aux formations qui les intéressent. Cette application à été faite en 4 parties :

La partie 1 permet de :
---
- Charger et implémenter les données des Villes et des départements dans lesquels il y a des IUT depuis un fichier texte, vers les structures VilleIUT et ListeDept.
- Charger et implémenter les données des Utilisateurs depuis un fichier texte ("user" qui contient pour chaque utilisateur, le nom, le prénom, le mot de passe et son type), vers la structure User.
- Mettre à jour / Sauvegarder les informations des IUT ainsi que des Utilisateurs dans leurs fichiers respectifs.
- Gérer le menu des fonctionnalités selon le type d'utilisateur (user,admin, responsable de département)                                     
- Afficher toutes les villes dans lesquels il y a un IUT.
- Afficher tous les départements pour une villeIUT donnée.                                     
- Empêcher un utilisateur d'accèder au menu admin si il n'en a pas les droits.
- Afficher toutes les villes des IUT qui possèdent un département donné.                                           
- Aux admins de modifier le nombre de place d'un IUT.                 
- Aux admins de créer ou supprimer un département dans une villeIUT
- Gérer les erreurs de saisie
- Gérer les autres erreurs (ville inexistante, mot de passe incorect...)
- Se connecter en tant qu'utilisateur ou admin
- S'inscrire 

Voici les structures que nous avons utilisées pour cette partie :
---
La structure maillon est utilisée pour créer une liste chaînée de départements d'une ville. <br>
La structure MaillonDept est un raccourci pour struct maillon et ListeDept est un pointeur vers MaillonDept. <br>
La structure User est utilisée pour stocker des informations sur les utilisateurs d'un système. 

Les structures maillon et MaillonDept permettent de stocker des informations sur les départements d'une ville, notamment leur nom, le nombre de places disponibles et le nom du responsable, ainsi que de stocker des informations sur les villes, notamment leur nom et leurs départements. La liste chaînée de départements permet de parcourir les différents départements d'une ville de manière efficace en utilisant un pointeur vers le prochain élément de la liste. <br>

La structure User permet de stocker des informations sur les utilisateurs de notre application, notamment leur nom, leur prénom, leur mot de passe et leur type d'utilisateur. L'identifiant unique permet de différencier les utilisateurs les uns des autres et faciliter la gestion des utilisateurs.

La partie 2 permet de :
---
- Charger et implémenter les données des candidatures depuis un fichier texte, vers les structures Candidat et ListeCandidature dans un fichier texte candidatures.txt qui contient le nombre totale de candidature, le numéro du candidat, son nom, son prénom, ses notes, son nombre de candidature, la ville, le département, la décision du département et sa décision finale.
- Sauvegarder les candidatures dans le fichier.
- Mettre dans le menu les fonctions respectives a cette partie.
- Afficher les candidatures
- Ajouter une candidature
- Supprimer une candidature
- Afficher les informations concernant un candidat 
- Afficher la liste des candidats

Voici les structures que nous avons utilisées pour cette partie :
---
La structure maillonC est utilisée pour créer une liste chaînée de candidatures d'un candidat. <br>
La structure MaillonCandidature est un raccourci pour struct maillonC et ListeCandidature est un pointeur vers MaillonCandidature. <br>

Ces structures permettent de stocker des informations sur les candidats à une formation d'IUT, notamment leur nom, leur prénom, leurs notes et leur moyenne, ainsi que des informations sur les candidatures des candidats, notamment les villes et les départements où ils postulent, les décisions prises par les départements et les décisions finales de l'IUT. La liste chaînée de candidatures permet de parcourir les différentes candidatures d'un candidat de manière efficace en utilisant un pointeur vers le prochain élément de la liste.


Répartition du travail
======================

**Léo :**
+ Tout les chargements
+ Toute les sauvegardes
+ Fonction de recherche dans les tableaux
+ Affichage du Menu et des sous-menus
+ Fonction pour le Menu (main)
+ Structures
+ Gestion des choix dans les différents menus
+ Etc...

**Yvan :**
+ Tout les chargements
+ Toute les sauvegarde
+ Fonction de recherche dans les tableaux
+ Affichage du Menu et des sous-menus
+ Fonction pour le Menu (main)
+ Structures
+ Gestion des choix dans les différents menus
+ Etc...


**Mathéo :**
+ Tout les chargements 
+ Toute les sauvegardes
+ Fonction de recherche dans les tableaux
+ Affichage du Menu et des sous-menus
+ Fonction pour le Menu (main)
+ Structures
+ Gestion des choix dans les différents menus
+ Doxygene de toute la SAE
+ Etc...

>Nous avons tout les trois pratiquement travaillés sur les mêmes aspects de la SAE.
>Se réferer a la doxygene pour plus de détails
