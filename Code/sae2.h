#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Structure


typedef enum { False, True } bool;

typedef struct maillon
{
    char departement[30];
    int NbPlaces;
    char responsable[30];
    struct maillon *suiv;
}MaillonDept, *ListeDept;

typedef struct
{
    char ville[30];
    ListeDept ldept;
}VilleIUT;

// struct des users
typedef struct
{
    int id;
    char nom[30];
    char prenom[30];
    char mdp[30];
    int type;
}User;


// struct partie 2
typedef struct maillonC
{
    char ville[30];
    char departement[30];
    int decisionDept;
    int decision;
    struct maillonC *suiv;
} MaillonCandidature, *ListeCandidature;

typedef struct
{
    int num;
    char nom[30];
    char prenom[30];
    float notes[4]; //Maths, Francais, Anglais, Matière Specialité
    float moy;
    ListeCandidature lCandidature;
}Candidat;


//========================Fonctions Partie 1========================// 
// Fonctions de gestions des listes : 
ListeDept listeVide(void);
ListeDept insererEnTete(ListeDept l, char departement[], int NbPlaces, char responsable[]);
ListeDept insertionCroissante(ListeDept l, char departement[], int nbP,char responsable[]);
int appartient(ListeDept ldept, char departement[], int nbP, char responsable[]);
int rechercheNbPlaces(ListeDept ldept, char departement[]);
void ModifMaillonPlace(ListeDept l, int nbPlaces, char departement[]);
ListeDept supprimerEnTete(ListeDept l);
ListeDept supprimer(ListeDept l, char departement[]);
void supprDept(VilleIUT *tiut[], int nb, char ville[], char departement[]);


// Fonctions de gestions des tableaux VilleIUT :
int nbPlaces(VilleIUT *tiut[],int nb, char ville[], char departement[]);
void triEchange(VilleIUT *tiut[],int nb);
void echanger(VilleIUT *tiut[],int i,int j);
int plusGrandElement(VilleIUT *tiut[],int nb);
void nouveauDept(VilleIUT *tiut[],int nb, char ville[], char departement[], int NbPlaces, char responsable[]);
void afficherVilleDeservant(VilleIUT *tiut[],int nb, char ville[]);
VilleIUT** chargement(char nomFic[], int *nbVilles, int *max);
int longueur (ListeDept l);
int rechercheDept(ListeDept ldept, char departement[]);
int nbPlaces(VilleIUT *tiut[],int nb, char ville[], char departement[]);
int rechercheNbPlaces(ListeDept ldept, char departement[]);
void modifNbPlaces(VilleIUT *tiut[],int nb, char ville[], char departement[], int nbPlaces);
void rechercheModifNbPlaces(ListeDept ldept, char departement[], int nbPlaces);


// Fonctions de gestions des tableaux Users :
// int rechercheDicoUser(User *tiut[],int nb, char nom[], char prenom[],int *trouve);
// User** chargementUsers(char nomFic[], int *nbUsers,int *max);
// User Lire1User(FILE *flot);
// int Connection(User *tUsers[], int nb, char nom[], char prenom[], char mdp[])

// fonctions des affichages :
void affichage(VilleIUT *tiut[],int nb);
void afficherUsers(User *tUsers[], int nb);
void afficherListeDept(ListeDept ldept);
void affichageVilleIUT(VilleIUT *tiut[],int nb, char departement[]);
void afficherVille(VilleIUT *tiut[],int nb);
void afficherDept(ListeDept ldept);
void afficherDeptIUT(VilleIUT *tiut[], int nb);
void afficherNbPlaces(VilleIUT *tiut[],int nb, char ville[],char departement[]);


// fonctions des menus :

void afficherMenuConnection(void);
void afficherMenu(int typeUtilisateur);
void GestionUser(VilleIUT *tiut[],char ville[],int nbVilles, char departement[], char responsable[], int typeUtilisateur);
void GestionAdmin(VilleIUT *tiut[],char ville[],int nbVilles, char departement[], char responsable[], int nbrPlaces,int typeUtilisateur);

// Fonctions de sauvegarde :
void libererUsers(User *tUsers[], int nb);
void sauvegardeUsers(User *tUsers[], int *nb);
void sauvegarde(VilleIUT *tiut[], int nb);
void sauvegardeDept(ListeDept l, FILE *f);
void sauvegardeCandidats(Candidat *tcand[], int nbCandidat);
void sauvegardeCandidatures(ListeCandidature l, FILE *f);
//===================================================================// 




//========================Fonctions Partie 2========================// 

// Fonctions des menus :
int choix_menu(void);
int choix_menu_user(void);
int choix_menu_admin(void);
void menu(void);

int menu_connection(VilleIUT *tiut[], Candidat *tcand[], User *tUsers[], int typeUtilisateur, int nbVilles, int nbcand, int nbUsers, int *OuvrirCandidatures, int maxUsers, int max);
int menu_user(VilleIUT *tiut[], Candidat *tcand[], User *tUsers[], int typeUtilisateur, int nbVilles, int nbcand, int nbUsers, int *OuvrirCandidatures, int num);
int menu_admin(VilleIUT *tiut[], Candidat *tcand[], User *tUsers[], int typeUtilisateur, int nbVilles, int nbcand, int nbUsers, int *OuvrirCandidatures, int nbrPlaces);


// Fonctions de gestions des listes :
ListeCandidature ajouterChoix(ListeCandidature lc, char ville[], char departement[], int decisionDept, int decision);
ListeCandidature insererChoixEnTete(ListeCandidature c, char ville[],char departement[], int decisionDept, int decision);
ListeCandidature insertionChoixCroissante(ListeCandidature c, char ville[],char departement[], int decisionDept, int decision);
int rechercheDicoCandidature(Candidat *cand[],int nb, int num, int *trouve);
int appartientChoix(ListeCandidature c, char ville[],char departement[], int decisionDept, int decision);
void afficherlisteCandidature(ListeCandidature lc);
void nouveauChoix(Candidat *cand[],int nb, int num, char nom[], char prenom[], float notes[],char ville[],char departement[], int decisionDept, int decision);
ListeCandidature supprimerEnTeteCand(ListeCandidature l);
void supprimerCandidatCand(Candidat *cand[],int *nb);
void modifierCandidat(Candidat *cand[],int nb);


Candidat* nouveauCandidature(ListeCandidature lCandidature,int *nbcand, int num, char nom[30], char prenom[30], float notes[4]);
ListeCandidature listeVideCandidature(void);
void afficherCandidat(Candidat *cand[], int nb);
int longueurCandidature(ListeCandidature l);

Candidat** chargementTest(char nomFic[], int *nbcand,int *max);
void affichageCandidature(Candidat *cand[],int nb);
void affichageChoix(ListeCandidature c);
int appartientVille(ListeCandidature c, char ville[]);

ListeCandidature supprimerEnTeteCand(ListeCandidature l);
Candidat **nouveauCandidat(Candidat *tcand[], int* nb, int *max, char nom[], char prenom[], float notes[]);

//==============================Test================================//
User** chargementUsers(char nomFic[], int *nbUsers,int *max);
int Connection(User *tUsers[], int nb, char nom[], char prenom[], char mdp[]);
//int Inscription(User *tUsers[], int *nb, int *max, char nom[], char prenom[], char mdp[], int type);
int rechercheDicoUser(User *tUsers[],int *nb, char nom[], char prenom[],int *trouve);
User **Inscription(User *tUsers[], int *nb, int *max, char nom[], char prenom[], char mdp[], int type);


ListeCandidature supprimerCand(ListeCandidature l, char departement[]);
void supprCandidature(Candidat *tcand[], int nb, int num, char departement[]);
void supprimerCandidaturePrecise(Candidat *cand[],int *nb);
ListeCandidature supprimerCandidature(ListeCandidature l, char *ville, char *departement);


//==============================Partie 3================================//
int appartientIutInfClermont(ListeCandidature l, char ville[], char dept[]);
Candidat filtrerCandidats(Candidat *cand[], int nb, int *nbClfInfo);
void afficher1Candidat(Candidat *cand);
void AfficherCandidatureParDept(Candidat *tcand[], int nbCand, char ville[], char dept[]);
void calculerMoyenne(Candidat *tcand[], int nbCand);
void trierCandidatParMoyenne(Candidat cand[], int nb);
