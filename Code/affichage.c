/**
 * @file affichage.c
 * @author Matheo Hersan / Yvan Calatayud / Leo Tuaillon
 * @brief L'ensemble des fonction d'affichage
 * @version 0.1
 * @date 2022-11-11
 * 
 * @copyright Copyright (c) 2022
 */

#include "sae2.h"


/**
 * @brief Affiche le menu pour les utilisateurs et les administrateurs
 * 
 * @param typeUtilisateur 
 */
void afficherMenu(int typeUtilisateur)
{
    printf("\n");
	printf("+------------+ \n");
	printf("|  Bonjour ! |\n") ;
	printf("+------------+ \n");
	printf("\n");
	printf("+-------------------------------------------------------------------------+\n");
	printf("| Que voulez-vous faire ? \t \t \t \t \t \t  | \n") ;
    if (typeUtilisateur == 1)
    {
        printf("|\t1. Afficher les villes avec un IUT \t \t \t \t  | \n");
        printf("|\t2. Afficher les départements dans chaque IUT \t \t \t  | \n");
        printf("|\t3. Afficher le nombre de places en première année \t \t  | \n");
        printf("|\t4. Rechercher les IUT possédant un département bien particulier   | \n");
        printf("|\t5. Afficher les candidatures \t \t \t \t \t  | \n");
        printf("|\t6. Modifier les candidatures \t \t \t \t \t  | \n");
        printf("|\t7. Ajouter une candidature \t \t \t \t \t  | \n");
        printf("|\t8. Supprimer une candidature \t \t \t \t \t  | \n");
        printf("|\t9. Quitter \t \t \t \t \t \t \t  | \n");
		printf("+-------------------------------------------------------------------------+\n");
    }
    
    if (typeUtilisateur == 2)
    {
        printf("|\t1. Afficher les villes avec un IUT \t \t \t \t  | \n");
        printf("|\t2. Afficher les départements dans chaque IUT \t \t \t  | \n");
        printf("|\t3. Afficher le nombre de places en première année \t \t  | \n");
        printf("|\t4. Rechercher les IUT possédant un département bien particulier   | \n");
        printf("|\t5. Modifier le nombre de places dans un département \t \t  | \n");
        printf("|\t6. Créer un département dans un IUT \t \t \t \t  | \n");
        printf("|\t7. Supprimer un département d'un IUT \t \t \t \t  | \n");
        printf("|\t8. Afficher les candidatures \t \t \t \t \t  | \n");
        printf("|\t9. Lancer et arrêter la phase de candidature \t \t \t  | \n");
        printf("|\t10. Modifier le nom du responsable d'un département \t \t  | \n");
        printf("|\t11. Quitter \t \t \t \t \t \t \t  | \n");
		printf("+-------------------------------------------------------------------------+\n");
    }
}

/**
 * @brief Affiche le menu de connexion
 * 
 */
void afficherMenuConnection(void)
{
	printf("\033[0;31m+------------+ \033[00m\n");
	printf("\033[0;31m|  Bonjour ! |\033[00m\n") ;
	printf("\033[0;31m+------------+ \033[00m\n");
	printf("+-------------------------------------------------------------------------+\n");
	printf("| Que voulez-vous faire ? \t \t \t \t \t \t  | \n") ;
    printf("|\t1. Se Connecter \t \t \t \t \t \t  | \n");
    printf("|\t2. S'inscrire \t \t \t \t \t \t \t  | \n");
    printf("|\t9. Quitter \t \t \t \t \t \t \t  | \n");
    printf("+-------------------------------------------------------------------------+\n");
}
