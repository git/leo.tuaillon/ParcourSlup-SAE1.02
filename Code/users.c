/**
 * @file users.c
 * @author Leo Tuaillon / Yvan Calatayud / Matheo Hersan
 * @brief Ensemble des fonctions pour les utilisateur
 * @version 0.1
 * @date 2022
 * 
 * @copyright Copyright (c) 2022
 */

#include "sae2.h"


/**
 * @brief Fonction qui permet de lire un utilisateur dans un fichier
 * 
 * @author Leo Tuaillon
 * @param flot le fichier
 */
User Lire1User(FILE *flot)
{
    User u;
    fscanf(flot, "%s %s %s %d",u.nom,u.prenom, u.mdp,&u.type);
    return u;
}

/**
 * @brief Fonction qui permet de charger les utilisateurs dans un tableau
 * 
 * @author Leo Tuaillon / Yvan Calatayud / Matheo Hersan
 * @param nomFic le nom du fichier
 * @param nbUsers le nombre d'utilisateurs
 * @param max la taille du tableau
 * @return User** le tableau d'utilisateurs
 */
User** chargementUsers(char nomFic[], int *nbUsers,int *max)
{   
    User u;
    int trouve, pos,i;
    
    User **tUsers ,**aux;
    FILE *f;
    f = fopen(nomFic, "r");
    if(f == NULL)
    {
        printf("Erreur d'ouverture du fichier\n");
        exit(1);
    }
    *nbUsers = 0;
    *max =5;
    tUsers = (User**)malloc(*max*sizeof(User*));
    if(tUsers == NULL)
    {
        printf("Erreur d'allocation mémoire\n");
        exit(1);
    }
    u = Lire1User(f);
    
    while(!feof(f))
    {
        pos = rechercheDicoUser(tUsers,nbUsers,u.nom,u.prenom,&trouve);
        if(trouve == 0)
        {
            if(*nbUsers == *max)    
            {
                *max = *max + 5;
                aux = (User **)realloc(tUsers,*max*sizeof(User*));
                if(aux==NULL)
                {
                    printf("Erreur de réallocation mémoire\n");
                    exit(1);
                }   
                tUsers = aux;
            }
            for(i=*nbUsers;i>pos;i--)
                tUsers[i] = tUsers[i-1];
            tUsers[pos] = (User*) malloc(sizeof(User));
            if(tUsers[pos]==NULL)
            {
                printf("Erreur d'allocation mémoire\n");
                exit(1);
            }
            strcpy(tUsers[pos]->nom,u.nom);
            strcpy(tUsers[pos]->prenom,u.prenom);
            strcpy(tUsers[pos]->mdp,u.mdp);
            tUsers[pos]->type = u.type;
            *nbUsers = *nbUsers+1;
        }
        u = Lire1User(f);
    }
    fclose(f);
    return tUsers;
}

/**
 * @brief Fonction qui permet de rechercher un utilisateur de manière dicotomique dans un tableau
 * 
 * @author Leo Tuaillon
 * @param tUsers le tableau d'utilisateurs
 * @param nb le nombre d'utilisateurs
 * @param nom le nom de l'utilisateur
 * @param prenom le prenom de l'utilisateur
 * @param trouve un entier qui permet de savoir si l'utilisateur a été trouvé
 */
int rechercheDicoUser(User *tUsers[],int *nb, char nom[], char prenom[],int *trouve)
{   

    int inf=0,sup=*nb-1,mil;
    while(inf<=sup)
    {
        mil = (inf+sup)/2;
        if(strcmp(nom,tUsers[mil]->nom)==0)
        {
            *trouve = 1;
            return mil;
        }
        if(strcmp(nom,tUsers[mil]->nom)<0)
            sup = mil-1;
        else
            inf = mil+1;
    }
    *trouve = 0;
    return inf;
}

/**
 * @brief Fonction qui permet d'afficher les utilisateurs
 * 
 * @author Leo Tuaillon
 * @param tUsers le tableau d'utilisateurs
 * @param nb le nombre d'utilisateurs
 */
void afficherUsers(User *tUsers[], int nb)
{
    int i;
    for(i=0;i<nb;i++)
        printf("%s\t%s\t%s\t%d\n", tUsers[i]->nom,tUsers[i]->prenom,tUsers[i]->mdp,tUsers[i]->type);
}

/**
 * @brief Fonction qui permet de sauvegarder les utilisateurs dans un fichier "users"
 * 
 * @author Leo Tuaillon / Yvan Calatayud / Matheo Hersan
 * @param tUsers le tableau d'utilisateurs
 * @param nb le nombre d'utilisateurs
 */
void sauvegardeUsers(User *tUsers[], int *nb)
{
    int i;
    FILE *f;
    f = fopen("users", "w");
    if(f == NULL)
    {
        printf("Erreur d'ouverture du fichier\n");
        exit(1);
    }

    for(i=0;i<*nb;i++)
    {
        fprintf(f, "%s %s %s %d\n", tUsers[i]->nom,tUsers[i]->prenom,tUsers[i]->mdp,tUsers[i]->type);
    }
    fclose(f);
}

/**
 * @brief Fonction qui permet de libérer la mémoire allouée pour les utilisateurs
 * 
 * @author Leo Tuaillon
 * @param tUsers le tableau d'utilisateurs
 * @param nb le nombre d'utilisateurs
 */
void libererUsers(User *tUsers[], int nb)
{
    int i;
    for(i=0;i<nb;i++)
        free(tUsers[i]);
    free(tUsers);
}

/**
 * @brief Fonction qui permet de se connecter
 * 
 * @author Leo Tuaillon
 * @param tUsers le tableau d'utilisateurs
 * @param nb le nombre d'utilisateurs
 * @param nom le nom de l'utilisateur
 * @param prenom le prenom de l'utilisateur
 * @param mdp le mot de passe de l'utilisateur
 * @return int le type de l'utilisateur
 */
int Connection(User *tUsers[], int nb, char nom[], char prenom[], char mdp[])
{
    int trouve, pos;
    pos = rechercheDicoUser(tUsers,&nb,nom,prenom,&trouve);
    if(trouve == 1)
    {
        if(strcmp(mdp,tUsers[pos]->mdp)==0)
            return tUsers[pos]->type;
        else
            return -1;
    }
    else
        return 0;
}

/**
 * @brief Fonction qui permet d'inscrire un utilisateur
 * 
 * @author Leo Tuaillon / Yvan Calatayud / Matheo Hersan
 * @param tUsers le tableau d'utilisateurs
 * @param nb le nombre d'utilisateurs
 * @param max le nombre maximum d'utilisateurs
 * @param nom le nom de l'utilisateur
 * @param prenom le prenom de l'utilisateur
 * @param mdp le mot de passe de l'utilisateur
 * @param type le type de l'utilisateur
 */
User **Inscription(User *tUsers[], int *nb, int *max, char nom[], char prenom[], char mdp[], int type)
{
    int trouve, pos,i;
    User **aux, *u;
    pos = rechercheDicoUser(tUsers,nb,nom,prenom,&trouve);
    
    if(trouve == 0)
    {
        if(*nb == *max)    
        {
            *max = *max + 5;
            aux = (User **)realloc(tUsers,*max*sizeof(User*));
            if(aux==NULL)
            {
                    printf("Erreur de réallocation mémoire\n");
                    exit(1);
            }   
            tUsers = aux;
        }
        for(i=*nb;i>pos;i--)
            tUsers[i] = tUsers[i-1];
        u = (User*) malloc(sizeof(User));
        if(u==NULL)
        {
            printf("Erreur d'allocation mémoire\n");
            exit(1);
        }
        
        strcpy(u->nom,nom);
        strcpy(u->prenom,prenom);
        strcpy(u->mdp,mdp);
        u->type = type;
        tUsers[pos] = u;
        *nb = *nb+1;
        printf("\nInscription réussie, veuillez désormais vous connecter :\n");
        return tUsers;
    }


    printf("Inscription échouée !\n");

    return tUsers;
    
}