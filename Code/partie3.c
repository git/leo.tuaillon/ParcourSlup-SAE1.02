/**
 * @file partie3.c
 * @brief Ensemble des fonctions pour la partie 3 du projet SAE
 * @version 0.1
 * @date 2022
 * 
 * @copyright Copyright (c) 2022
 */


#include"sae2.h"

/**
 * @brief Fonction qui calcule la moyenne de chaque Candidat
 *
 * @author Leo Tuaillon / Mathéo Hersan / Yvan Calatayud
 * @param tcand Tableau de pointeurs sur Candidat
 * @param nbCand Nombre de Candidats
 */
void calculerMoyenne(Candidat *tcand[], int nbCand)
{
    int i;
    for(i=0;i<nbCand;i++)
    {
        tcand[i]->moy = (tcand[i]->notes[0]*16 + tcand[i]->notes[1]*10 + tcand[i]->notes[2]*5 + tcand[i]->notes[3]*16)/4;
    }
}

/**
 * @brief Fonction qui affiche les candidatures par département
 *
 * @author Leo Tuaillon
 * @param tcand Pointeur sur Candidat
 * @param nbCand Nombre de Candidats
 * @param ville Ville de l'IUT
 * @param dept Département de l'IUT
 */
void AfficherCandidatureParDept(Candidat *tcand[], int nbCand, char ville[], char dept[])
{
    int i;
    for(i=0;i<nbCand;i++)
    {
        if(strcmp(tcand[i]->lCandidature->ville,ville)==0 && strcmp(tcand[i]->lCandidature->departement,dept)==0)
        {
            afficher1Candidat(tcand[i]);
        }
    }
}

/**
 * @brief Fonction qui affiche les informations d'un Candidat
 * 
 * @author Leo Tuaillon
 * @param cand Pointeur sur Candidat
 */
void afficher1Candidat(Candidat *cand)
{
    printf("num : %d\nnom : %s\nprenom : %s\nmoyenne : %f\n\n",cand->num,cand->nom,cand->prenom,cand->moy);
}

/**
 * @brief Fonction qui filtre les Candidats et les trie par ordre décroissant de moyenne
 * 
 * @author Leo Tuaillon
 * @param cand Pointeur sur Candidat
 * @param nb Nombre de Candidats
 * @param nbClfInfo Nombre de Candidats filtrés
 */
Candidat filtrerCandidats(Candidat *cand[], int nb, int *nbClfInfo)
{
    int i, j;
    char ville[20]="Clermont-Ferrand", departement[20]="Informatique";
    Candidat candClfInfo[nb];
    int nbCandClfInfo = 0;

    // Parcours toutes les Candidats pour ne garder que celles qui ont le département donné en paramètre
    for (i = 0; i < nb; i++)
    {   
        if(appartientIutInfClermont(cand[i]->lCandidature, ville,departement))
        {
            candClfInfo[nbCandClfInfo] = *cand[i];
            nbCandClfInfo=nbCandClfInfo+1;
        }
    }

    // Tri des Candidats en fonction de la note
    for (i = 0; i < nbCandClfInfo; i++)
    {
        for (j = i + 1; j < nbCandClfInfo; j++)
        {
            if (candClfInfo[i].notes[3] < candClfInfo[j].notes[3])
            {
                Candidat temp = candClfInfo[i];
                candClfInfo[i] = candClfInfo[j];
                candClfInfo[j] = temp;
            }
        }
    }
    return *candClfInfo;
}

/**
 * @brief Fonction qui garde uniquement les Candidats de l'IUT de Clermont-Ferrand
 * 
 * @author Leo Tuaillon
 * @param candClfInfo Pointeur sur Candidat
 * @param nb Nombre de Candidats
 */
void garderMaillonClermont(Candidat candClfInfo[],int nb)
{
    for(int i=0;i<nb;i++)
    {
        while (1)
        {
            if (strcmp(candClfInfo->lCandidature->ville, "Clermont-Ferrand") == 0 && strcmp(candClfInfo[i].lCandidature->departement, "Informatique") == 0)
            {
                candClfInfo[i].lCandidature->suiv = NULL;
                return;
            }
            candClfInfo[i].lCandidature = candClfInfo[i].lCandidature->suiv;
        }
    }
}

/**
 * @brief Fonction qui vérifie si un Candidat appartient à l'IUT de Clermont-Ferrand
 * 
 * @author Leo Tuaillon
 * @param l Pointeur sur ListeCandidature
 * @param ville Ville de l'IUT
 * @param dept Département de l'IUT
 * @return int 1 si le Candidat appartient à l'IUT, 0 sinon
 */
int appartientIutInfClermont(ListeCandidature l, char ville[], char dept[])
{
    while (l != NULL)
    {
        if (strcmp(l->ville, ville) == 0 && strcmp(l->departement, dept) == 0)
        {
            return 1;
        }
        l = l->suiv;
    }
    return 0;
}

/**
 * @brief Fonction qui trie les Candidats par ordre décroissant de moyenne
 * 
 * @author Leo Tuaillon
 * @param candClfInfo Pointeur sur Candidat
 * @param nb Nombre de Candidats
 */
void trierCandidatParMoyenne(Candidat candClfInfo[], int nb)
{
    int i, j;
    for (i = 0; i < nb; i++)
    {
        for (j = i + 1; j < nb; j++)
        {
            if (candClfInfo[i].moy < candClfInfo[j].moy)
            {
                Candidat temp = candClfInfo[i];
                candClfInfo[i] = candClfInfo[j];
                candClfInfo[j] = temp;
            }
        }
    }
}


/**
 * @brief Fonction qui enregistre les Candidats en liste d'attente dans un fichier
 * 
 * @author Leo Tuaillon / Mathéo Hersan / Yvan Calatayud
 * @param candAttente Pointeur sur Candidat
 * @param nbCandAttente Nombre de Candidats en liste d'attente
 */
void enregistrerListeAttente(Candidat candAttente[], int nbCandAttente)
{
    FILE *fp;
    int i;

    // Ouvrir un fichier en mode écriture
    fp = fopen("liste_attente.txt", "w");
    if (fp == NULL)
    {
        printf("Erreur lors de l'ouverture du fichier\n");
        return;
    }

    // Écrire les informations des Candidatures en liste d'attente dans le fichier
    for (i = 0; i < nbCandAttente; i++)
    {
        fprintf(fp, "Numéro de Candidat : %d\n", candAttente[i].num);
        fprintf(fp, "Nom : %s\n", candAttente[i].nom);
        fprintf(fp, "Prénom : %s\n", candAttente[i].prenom);
        fprintf(fp, "Note : %.2f\n", candAttente[i].moy);
        fprintf(fp, "Ville : %s\n", candAttente[i].lCandidature->ville);
        fprintf(fp, "Département : %s\n", candAttente[i].lCandidature->departement);
        fprintf(fp, "Décision du département : %d\n", candAttente[i].lCandidature->decisionDept);
        fprintf(fp, "Décision finale : %d\n\n", candAttente[i].lCandidature->decision);
    }

    // Fermer le fichier
    fclose(fp);
    printf("La liste d'attente a été enregistrée dans le fichier liste_attente.txt\n");
}
