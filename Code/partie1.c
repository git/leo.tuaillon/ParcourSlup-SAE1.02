/**
 * @file partie1.c
 * @brief Ensemble des fonctions pour la partie 1 du projet SAE
 * @version 0.1
 * @date 2022
 * 
 * @copyright Copyright (c) 2022
 */

#include "sae2.h"

/**
 * @brief Fonction de création d'une liste vide de départements
 *
 * @author Yvan Calatayud
 * @return une liste de départements vide
 */
ListeDept listeVide(void)
{
    return NULL;
}

/**
 * @brief Fonction qui permet d'insérer un département en tête d'une liste de départements.
 *
 * @author Yvan Calatayud/Leo Tuaillon/Mathéo Hersan
 * @param l : liste de départements dans laquelle on veut insérer le département
 * @param departement : nom du département à insérer
 * @param nbP : nombre de places dans le département
 * @param responsable : nom du responsable du département
 * @return la liste de départements avec le département inséré en tête
 */
ListeDept insererEnTete(ListeDept l, char departement[], int nbP,char responsable[])
{
    MaillonDept *m;
    m = (MaillonDept *)malloc(sizeof(MaillonDept));
    if (m == NULL)
    {
        printf("Erreur malloc\n");
        exit(1);
    }
    strcpy(m->departement, departement);
    m->NbPlaces = nbP;
    strcpy(m->responsable, responsable);
    m->suiv = l;
    return m;
}

/**
 * @brief Fonction qui permet d'insérer un département dans une liste de départements de manière à ce que la liste reste triée par ordre alphabétique croissant.
 *
 * @author Yvan Calatayud/Leo Tuaillon/Mathéo Hersan
 * @param ldept : liste de départements dans laquelle on veut insérer le département
 * @param departement : nom du département à insérer
 * @param nbP : nombre de places dans le département
 * @param responsable : nom du responsable du département
 * @return la liste de départements avec le département inséré à sa place correspondante
 */
ListeDept insertionCroissante(ListeDept ldept, char departement[], int nbP,char responsable[])
{
    if (ldept == NULL)
        return insererEnTete(ldept, departement, nbP, responsable);
    if (strcmp(departement,ldept-> departement) < 0)
        return insererEnTete(ldept, departement, nbP, responsable);
    if (strcmp(departement, ldept->departement) == 0)
        return ldept;
    ldept -> suiv = insertionCroissante(ldept->suiv, departement, nbP, responsable);
    return ldept;
}

/**
 * @brief Fonction qui permet de vérifier si un département appartient à une liste de départements.
 * 
 * @author Yvan Calatayud/Leo Tuaillon/Mathéo Hersan
 * @param ldept : liste de départements dans laquelle on cherche le département
 * @param departement : nom du département à rechercher
 * @param nbP : nombre de places dans le département (non utilisé dans la fonction)
 * @param responsable : nom du responsable du département (non utilisé dans la fonction)
 * @return 1 si le département appartient à la liste, 0 sinon
 */
int appartient(ListeDept ldept, char departement[], int nbP, char responsable[])
{
    if (ldept == NULL)
        return 0;
    if (strcmp(departement, ldept->departement) < 0)
        return 0;
    if (strcmp(departement, ldept->departement) == 0)
        return 1;
    return appartient(ldept->suiv, departement, nbP, responsable);
}

/**
 * @brief Fonction qui trie un tableau de villes par ordre alphabétique croissant en utilisant l'algorithme du tri par échange.
 *
 * @author Yvan Calatayud/Leo Tuaillon/Mathéo Hersan
 * @param tiut : tableau de villes à trier
 * @param nb : nombre de villes dans le tableau
 */
void triEchange(VilleIUT *tiut[],int nb)
{
    int pge;
    while(nb>1)
    {
        pge = plusGrandElement(tiut,nb);
        echanger(tiut,pge,nb-1);
        nb=nb-1;
    }
}

/**
 * @brief Fonction qui échange deux villes dans un tableau.
 *
 * @author Yvan Calatayud/Leo Tuaillon/Mathéo Hersan
 * @param tiut : tableau de villes dans lequel on veut échanger les villes
 * @param i : indice de la première ville à échanger
 * @param j : indice de la deuxième ville à échanger
 */
void echanger(VilleIUT *tiut[],int i,int j)
{
    VilleIUT *tmp;
    tmp = tiut[i];
    tiut[i] = tiut[j];
    tiut[j] = tmp;
}

/**
 * @brief Fonction qui cherche la ville la plus grande (selon l'ordre alphabétique) dans un tableau de villes.
 *
 * @author Yvan Calatayud/Leo Tuaillon/Mathéo Hersan
 * @param tiut : tableau de villes dans lequel on cherche la ville la plus grande
 * @param nb : nombre de villes dans le tableau
 * @return l'indice de la ville la plus grande dans le tableau
 */
int plusGrandElement(VilleIUT *tiut[],int nb)
{
    int i, pge;
    pge = 0;
    for(i=1;i<nb;i++)
    {
        if(strcmp(tiut[i]->ville,tiut[pge]->ville)>0)
            pge = i;
    }
    return pge;
}

/**
 * @brief Fonction qui cherche une ville dans un tableau de villes en utilisant la recherche dichotomique.
 *
 * @author Yvan Calatayud/Leo Tuaillon/Mathéo Hersan
 * @param tiut : tableau de villes dans lequel on cherche la ville
 * @param nb : nombre de villes dans le tableau
 * @param ville : nom de la ville à chercher
 * @param trouve : pointeur vers un entier qui indique si la ville a été trouvée (1) ou non (0)
 * @return l'indice de la ville dans le tableau s'il a été trouvé, l'indice où la ville devrait être insérée sinon
 */
int rechercheDico(VilleIUT *tiut[],int nb, char ville[],int *trouve)
{
    int inf=0,sup=nb-1,mil;
    while(inf<=sup)
    {
        mil = (inf+sup)/2;
        if(strcmp(ville,tiut[mil]->ville)==0)
        {
            *trouve = 1;
            return mil;
        }
        if(strcmp(ville,tiut[mil]->ville)<0)
            sup = mil-1;
        else
            inf = mil+1;
    }
    *trouve = 0;
    return inf;
}

/**
 * @brief Fonction qui ajoute un département à une ville dans un tableau de villes. Si la ville n'existe pas ou si le département existe déjà pour cette ville, un message d'erreur est affiché.
 *
 * @author Yvan Calatayud/Leo Tuaillon/Mathéo Hersan
 * @param tiut : tableau de villes dans lequel on veut ajouter le département
 * @param nb : nombre de villes dans le tableau
 * @param ville : nom de la ville à laquelle on veut ajouter le département
 * @param departement : nom du département à ajouter
 * @param NbPlaces : nombre de places dans le département
 * @param responsable : nom du responsable du département
 */
void nouveauDept(VilleIUT *tiut[],int nb, char ville[], char departement[], int NbPlaces, char responsable[])
{
    int trouve, pos;
    pos = rechercheDico(tiut,nb,ville,&trouve);
    if(trouve == 0)
    {
        printf("Ville inconnue\n");
        return;
    }
    if(appartient(tiut[pos]->ldept,departement,NbPlaces,responsable))
    {
        printf("Departement deja existant\n");
        return;
    }
    tiut[pos]->ldept = insertionCroissante(tiut[pos]->ldept,departement,NbPlaces,responsable);
}

/**
 * @brief Fonction qui vérifie si une liste de départements est vide ou non.
 *
 * @author Yvan Calatayud/Leo Tuaillon/Mathéo Hersan
 * @param l : liste de départements à vérifier
 * @return True si la liste est vide, False sinon
 */
bool vide(ListeDept l)
{
    if (l == NULL)
        return True;
    return False;
}

/**
 * @brief Fonction qui calcule la longueur d'une liste de départements (c'est-à-dire le nombre de départements dans la liste).
 *
 * @author Yvan Calatayud/Leo Tuaillon/Mathéo Hersan
 * @param l : liste de départements dont on veut calculer la longueur
 * @return le nombre de départements dans la liste
 */
int longueur (ListeDept l)
{
    int cpt = 0;
    while (!vide(l))
    {
        cpt++;
        l = l->suiv;
    }
    return cpt;
}

/**
 * @brief Fonction qui charge les données à partir d'un fichier et les stocke dans une structure de données.
 *
 * @authors Yvan Calatayud/Leo Tuaillon/Mathéo Hersan
 * @param nomFic : nom du fichier à partir duquel on veut charger les données
 * @param nbVilles : nombre de villes chargées
 * @param max : nombre maximum de villes pouvant être stockées
 * @return un tableau de pointeurs sur les villes chargées
 */
VilleIUT** chargement(char nomFic[], int *nbVilles,int *max)
{   
    int trouve, pos,i,nbPlaces;
    char ville[30],departement[30],responsable[30];
    FILE *f;

    f = fopen(nomFic, "r");
    if(f == NULL)
    {
        printf("Erreur d'ouverture du fichier\n");
        exit(1);
    }

    *nbVilles = 0;
    *max =5;
    VilleIUT **tiut ,**aux;
    tiut = (VilleIUT**)malloc(sizeof(VilleIUT*));
    if(tiut == NULL)
    {
        printf("Erreur d'allocation mémoire\n");
        exit(1);
    }
    fscanf(f,"%s",ville);
    while(!feof(f))
    {
        pos = rechercheDico(tiut,*nbVilles,ville,&trouve);
        if(trouve == 0)
        {
            if(*nbVilles == *max)
            {
                *max = *max + 5;
                aux = (VilleIUT **)realloc(tiut,*max*sizeof(VilleIUT*));
                if(aux==NULL)
                {
                        printf("Erreur de réallocation mémoire\n");
                        exit(1);
                }   
                tiut = aux;
            }
            for(i=*nbVilles;i>pos;i--)
                tiut[i] = tiut[i-1];
            tiut[pos] = (VilleIUT*) malloc(sizeof(VilleIUT));
            if(tiut[pos]==NULL)
            {
                printf("Erreur d'allocation mémoire\n");
                exit(1);
            }
            strcpy(tiut[pos]->ville,ville);
            tiut[pos]->ldept = listeVide();
            *nbVilles = *nbVilles+1;

        }
        fscanf(f,"%s %d%*c",departement,&nbPlaces);
        fgets(responsable,30,f);
        responsable[strlen(responsable)-1] = '\0';
        nouveauDept(tiut,*nbVilles,ville,departement,nbPlaces,responsable);
        fscanf(f,"%s",ville);
    }

    fclose(f);
    return tiut;
}

/**
 * @brief Fonction qui affiche toutes les villes et leurs départements dans un tableau de villes.
 *
 * @author Yvan Calatayud/Leo Tuaillon/Mathéo Hersan
 * @param tiut : tableau de villes à afficher
 * @param nb : nombre de villes dans le tableau
 */
void affichage(VilleIUT *tiut[],int nb)
{
    int i;
    for(i=0;i<nb;i++)
    {
        printf("%s : \n",tiut[i]->ville);
        afficherListeDept(tiut[i]->ldept);
    }
}

/**
 * @brief Fonction récursive qui affiche tous les départements d'une liste de départements.
 *
 * @author Yvan Calatayud/Leo Tuaillon/Mathéo Hersan
 * @param ldept : liste de départements à afficher
 */
void afficherListeDept(ListeDept ldept)
{
    if (ldept == NULL)
    {
        printf("\n");
        return;
    }  
    printf("%s\t%d places\t%s\n",ldept->departement,ldept->NbPlaces,ldept->responsable);
    afficherListeDept(ldept->suiv);
}

/**
 * @brief Fonction permettant d'afficher les départements d'un IUT en parcourant un tableau de structures VilleIUT.
 *
 * @author Yvan Calatayud
 * @param tiut : tableau de structures VilleIUT contenant les villes et les départements à afficher
 * @param nb : nombre d'éléments dans le tableau tiut
 */
void afficherDeptIUT(VilleIUT *tiut[], int nb)
{
    int i;
    for(i=0;i<nb;i++)
    {
        printf("\n%s : \n\n",tiut[i]->ville);
        afficherDept(tiut[i]->ldept);
    }
}

/**
 * @brief Fonction récursive qui affiche tous les départements d'une liste de départements.
 *
 * @author Yvan Calatayud
 * @param ldept : liste de départements à afficher
 */
void afficherDept(ListeDept ldept) // Affiche tous les departements
{
    if (ldept == NULL)
    {
        printf("\n");
        return;
    }  
    printf("\t%s\n",ldept->departement);
    afficherDept(ldept->suiv);
}

/**
 * @brief Fonction qui parcourt un tableau de structures VilleIUT et affiche les villes qui contiennent le département donné en paramètre.
 *
 * @author Hersan Mathéo
 * @param tiut : tableau de structures VilleIUT
 * @param nb : nombre d'éléments dans le tableau
 * @param departement : nom du département recherché
 */
void affichageVilleIUT(VilleIUT *tiut[],int nb, char departement[])
{
    int i;
    for(i=0;i<nb;i++)
    {
        if(rechercheDept(tiut[i]->ldept,departement))
            printf("\t%s \n",tiut[i]->ville);
    }
}

/**
 * @brief Fonction d'affichage de toutes les villes contenues dans un tableau de VilleIUT. 
 *
 * @author Leo Tuaillon
 * @param tiut : tableau de VilleIUT contenant les villes à afficher
 * @param nb : nombre de villes dans le tableau
 */
void afficherVille(VilleIUT *tiut[],int nb) // Affiche toutes les villes
{
    int i;
    printf("\nVilles : \n\n");
    for(i=0;i<nb;i++)
    {
        printf("\t%s \n",tiut[i]->ville);
    }
    printf("\n");
}

/**
 * @brief Fonction récursive qui parcourt une liste de départements et vérifie si un département donné en paramètre se trouve dans cette liste.
 * 
 * @author Leo Tuaillon
 * @param ldept : liste de départements dans laquelle on recherche
 * @param departement : département que l'on cherche
 * @return int : 1 si le département est trouvé, 0 sinon
 */
int rechercheDept(ListeDept ldept, char departement[]) 
{
    if (ldept == NULL)
        return 0;
    if (strcmp(ldept->departement,departement) == 0)
        return 1;
    return rechercheDept(ldept->suiv,departement);
}

/**
 * @brief Fonction d'affichage du nombre de places dans un département selon les villes
 *
 * @author Yvan Calatayud / Mathéo Hersan / Leo Tuaillon
 * @param tiut : tableau de VilleIUT
 * @param nb : nombre d'éléments dans tiut
 * @param ville : ville à rechercher
 * @param departement : département pour lequel on veut afficher le nombre de places
 */
void afficherNbPlaces(VilleIUT *tiut[],int nb, char ville[],char departement[]) // Affiche le nombres de places dans un département selon les villes
{
    int pos,trouve;
    pos = rechercheDico(tiut,nb,ville,&trouve);
    if(trouve == 1)
    {
        printf("\n%s : \n\n",tiut[pos]->ville);
        while(tiut[pos]->ldept != NULL)
        {
            if(strcmp(tiut[pos]->ldept->departement,departement) == 0)
            {
                printf("\t%s : %d places\n",departement,tiut[pos]->ldept->NbPlaces);
                return;
            }
            tiut[pos]->ldept = tiut[pos]->ldept->suiv;
        }
    }
}

/**
 * @brief Fonction de modification du nombre de places dans un département d'une ville donnée
 *
 * @author Leo Tuaillon/Mathéo Hersan/Yvan Calatayud
 * @param tiut : Tableau de VilleIUT (Liste des IUT et départements)
 * @param nb : Nombre d'éléments dans le tableau tiut
 * @param ville : Ville où se trouve le département
 * @param departement : Département où le nombre de places doit être modifié
 * @param nbPlaces : Nouveau nombre de places dans le département
 */
void modifNbPlaces(VilleIUT *tiut[], int nb, char ville[], char departement[], int nbPlaces)
{
    int pos,trouve;
    pos = rechercheDico(tiut,nb,ville,&trouve);
    if(trouve == 1)
    {
       
        ModifMaillonPlace(tiut[pos]->ldept,nbPlaces,departement);
    }
}

/**
 * @brief Fonction qui permet de modifier le nombre de places dans un département spécifié
 *
 * @author Yvan Calatayud/Leo Tuaillon/Mathéo Hersan
 * @param l : liste de départements
 * @param nbPlaces : nouveau nombre de places
 * @param departement : département à modifier
 */
void ModifMaillonPlace(ListeDept l, int nbPlaces, char departement[])
{
    while(l != NULL)
    {
        if(strcmp(l->departement,departement) == 0)
        {
            l->NbPlaces = nbPlaces;
            return;
        }
        l = l->suiv;
    }
}

/**
 * @brief Fonction de suppression d'un département d'une ville donnée
 *
 * @author Yvan Calatayud/Leo Tuaillon/Mathéo Hersan
 * @param tiut : tableau de villes et IUTs
 * @param nb : nombre de villes dans le tableau
 * @param ville : nom de la ville où supprimer le département
 * @param departement : nom du département à supprimer
 */
void supprDept(VilleIUT *tiut[], int nb, char ville[], char departement[])
{
    int pos,trouve;
    pos = rechercheDico(tiut,nb,ville,&trouve);
    if(trouve == 1)
    {
        supprimer(tiut[pos]->ldept,departement);
    }
}

/**
 * @brief Fonction de suppression d'un département selon les villes
 *
 * @author Mathéo Hersan
 * @param l : liste de départements à modifier
 * @param departement : nom du département à rechercher
 * @return ListeDept : liste de départements modifiée
 */
ListeDept supprimerEnTete(ListeDept l)
{
    MaillonDept *aux;
    if (l == NULL)
    {
        printf("Erreur de suppression en tete");
        exit(1);
    }
    aux = l;
    l = l->suiv;
    free(aux);
    return l;
}

/**
 * @brief Fonction de suppression d'un département selon les noms
 *
 * @author Yvan Calatayud  
 * @param l : liste de départements à modifier
 * @param departement : nom du département à rechercher
 * @return ListeDept : liste de départements modifiée
 */
ListeDept supprimer(ListeDept l, char departement[])
{
    if (l == NULL)
    {
        printf("Erreur de suppression");
        exit(1);
    }
    if (strcmp(l->departement,departement) > 0)
        return l;
    if (strcmp(l->departement,departement) == 0)
        return supprimerEnTete(l);
    l->suiv = supprimer(l->suiv,departement);
    return l;
}

/**
 * @brief Fonction qui sauvegarde dans un fichier toutes les informations relatives aux villes et départements.
 *
 * @author Mathéo Hersan
 * @date 2022
 * @param tiut : tableau des villes à sauvegarder
 * @param nb : nombre d'éléments dans le tableau
 */
void sauvegarde(VilleIUT *tiut[], int nb)
{
    FILE *f;
    int i;
    f = fopen("IUT.txt","w");
    if(f == NULL)
    {
        printf("Erreur d'ouverture du fichier");
        exit(1);
    }
    for(i=0;i<nb;i++)
    {
        while(tiut[i]->ldept != NULL)
        {
            fprintf(f,"%s\t",tiut[i]->ville);
            sauvegardeDept(tiut[i]->ldept,f);
            tiut[i]->ldept = tiut[i]->ldept->suiv;
        }
    }
    fclose(f);
}

/**
 * @brief Fonction qui sauvegarde dans un fichier toutes les informations relatives aux départements.
 *    
 * @author Hersan Mathéo
 * @date 2022
 * @param l : liste des départements à sauvegarder
 * @param f : fichier dans lequel sauvegarder
 */
void sauvegardeDept(ListeDept l, FILE *f)
{
    if(l == NULL)
        return;
    fprintf(f,"%s\t%d\t%s\n",l->departement,l->NbPlaces,l->responsable);
}
