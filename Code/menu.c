/**
 * @file menu.c
 * @author Mathéo Hersan, Yvan Calatayud, Leo Tuaillon
 * @brief Ensemble des fonctions pour la gestion du menu
 * @date 2022
 * 
 * @copyright Copyright (c) 2022
 */

#include"sae2.h"

/**
 * @brief Fonction des choix du menu user
 * 
 */
int choix_menu_user(void)
{
    int choix;
    printf("Saisir votre choix : ");
    scanf("%d", &choix);
    while ((choix < 1 || choix > 7) && choix != 9)
    {
        printf("Erreur, vous devez saisir un nombre entre 1 et 7 ou 9 pour quitter.\n");
        printf("Veuillez re saisir : ");
        scanf("%d", &choix);
    }

    return choix;
}

/**
 * @brief Fonction des choix du menu admin
 * 
 */
int choix_menu_admin(void)
{
    int choix;
    printf("Saisir votre choix : ");
    scanf("%d", &choix);
    while ((choix < 1 || choix > 9) && choix != 11)
    {
        printf("Erreur, vous devez saisir un nombre entre 1 et 9 ou 11 pour quitter.\n");
        printf("Veuillez re saisir : ");
        scanf("%d", &choix);
    }
    return choix;
}

/**
 * @brief Fonction des choix du menu de connection
 * 
 */
int choix_menu_connection(void)
{
    int choix;
    printf("Saisir votre choix : ");
    scanf("%d", &choix);
    while ((choix < 1 || choix > 2) && choix != 9)
    {
        printf("Erreur, vous devez saisir un nombre entre 1 et 2 ou 9 pour quitter.\n");
        afficherMenuConnection();
        printf("Veuillez re saisir : ");
        scanf("%d", &choix);
    }
    return choix;
}


/**
 * @brief Fonction pour afficher le menu
 * 
 */  
void menu(void)
{
    int nbVilles = 0,maxVilles, maxUsers, nbrPlaces,nbUsers, num, OuvrirCandidatures = 1;
    int max, nbcand=0;
    VilleIUT **tiut = chargement("IUT.txt", &nbVilles,&maxVilles);
    User **tUsers = chargementUsers("users", &nbUsers, &maxUsers);
    Candidat **tcand = chargementTest("candidatures.txt", &nbcand, &max);


    int typeUtilisateur=0;

    

    while(1)
    {
        
        if(typeUtilisateur == 0) // Menu connection
            typeUtilisateur = menu_connection(tiut, tcand, tUsers, typeUtilisateur, nbVilles, nbcand, nbUsers, &OuvrirCandidatures, maxUsers, max);
        
        if(typeUtilisateur == 1) // Menu user
            typeUtilisateur = menu_user(tiut, tcand, tUsers, typeUtilisateur, nbVilles, nbcand, nbUsers, &OuvrirCandidatures,num);
        
        else if(typeUtilisateur == 2) // Menu admin
            typeUtilisateur = menu_admin(tiut, tcand, tUsers, typeUtilisateur, nbVilles, nbcand, nbUsers, &OuvrirCandidatures, nbrPlaces);
        
        // else if(typeUtilisateur == 3) // Menu responsable
        // {
        //     menu_responsable();
        // }
    }
}

/**
 * @brief Fonction pour afficher le menu connexion
 * 
 */
int menu_connection(VilleIUT *tiut[], Candidat *tcand[], User *tUsers[], int typeUtilisateur, int nbVilles, int nbcand, int nbUsers, int *OuvrirCandidatures, int maxUsers, int max)
{
    int choix = 0;
    char nom[30], prenom[30], mdp[30];
    float notes[4];
    while(choix != 9 && choix != -1)
    {
        afficherMenuConnection(); // Affiche soit le menu user soit le menu admin suivant la variable typeUtilisateur
        choix = choix_menu_connection();

        switch(choix)
        {
            case 1:
                printf("saisir nom : ");
                scanf("%s",nom);
                printf("saisir prenom : ");
                scanf("%s",prenom);
                printf("saisir mdp : ");
                scanf("%s",mdp);
                typeUtilisateur = Connection(tUsers,nbUsers,nom,prenom,mdp);

                if(typeUtilisateur == 0)
                    printf("Nom ou prénom incorrect !\n");
                else if(typeUtilisateur == -1)
                    printf("Mot de passe incorrect !\n");
                
                
                
                return typeUtilisateur;
                break;
    
            case 2:
                printf("saisir nom (pas d'espace, si besoin mettre des traits d'union) : ");
                scanf("%s%*c",nom);
                printf("saisir prenom (pas d'espace, si besoin mettre des traits d'union) : ");
                scanf("%s%*c",prenom);
                printf("saisir mdp : ");
                scanf("%s%*c",mdp);
                tcand = nouveauCandidat(tcand, &nbcand, &max, nom, prenom,notes);
                tUsers = Inscription(tUsers, &nbUsers, &maxUsers, nom, prenom, mdp, 1);
                
                afficherUsers(tUsers, nbUsers);
        
                break;
                
            case 9:
                sauvegarde(tiut, nbVilles);
                sauvegardeUsers(tUsers, &nbUsers);
                sauvegardeCandidats(tcand, nbcand);
                exit(1);
        }
    }
}

/**
 * @brief Fonction pour afficher le menu user
 * 
 */
int menu_user(VilleIUT *tiut[], Candidat *tcand[], User *tUsers[], int typeUtilisateur, int nbVilles, int nbcand, int nbUsers, int *OuvrirCandidatures, int num)
{
    int choix=0;
    char departement[30], ville[30], nom[30], prenom[30];
    float notes[4];

    while(choix != 9)
    {
        afficherMenu(typeUtilisateur);
        choix = choix_menu_user();

        switch(choix)
        {
            case 1:
                afficherVille(tiut, nbVilles); // Affiche toutes les villes
                break;
                
            case 2:
                afficherDeptIUT(tiut,nbVilles); // Affiche les départements de chaque IUT
                break;

            case 3:
                printf("Saisir la ville : ");
                scanf("%s",ville);
                printf("Saisir le département : ");
                scanf("%s",departement);
                afficherNbPlaces(tiut,nbVilles,ville,departement); 
                break;

            case 4:
                printf("\nsaisir département : \n\n");
                scanf("%s",departement);
                printf("\n");
                affichageVilleIUT(tiut,nbVilles,departement); // Affiche les IUT d'un département bien précis
                break;
            
            case 5:// Afficher les candidatures
                affichageCandidature(tcand, nbcand);
                break;


            case 6: // Ajouter une Candidature
                if(*OuvrirCandidatures == 1)
                {
                    printf("Saisir le numéro : ");
                    scanf("%d", &num);
                    printf("Saisir la ville : ");
                    scanf("%s",ville);
                    printf("Saisir le département : ");
                    scanf("%s",departement);
                    nouveauChoix(tcand,nbcand,num,nom,prenom,notes,ville,departement,0,0); // Permet de modifier le choix d'un étudiant
                }
                    
                else if(*OuvrirCandidatures == 2)
                    printf("La phase de candidature est fermée\n");
                
                break;

            case 7: //supprimer une Candidature (attention supprime tout)
                if(*OuvrirCandidatures == 1)
                {
                    affichageCandidature(tcand, nbcand);
                    supprimerCandidaturePrecise(tcand, &nbcand);
                    affichageCandidature(tcand, nbcand);
                    printf("Candidature supprimée !\n");
                }
                        
                else if(*OuvrirCandidatures == 2)
                    printf("La phase de candidature est fermée\n");

                break;

            case 9: // Exit
                return typeUtilisateur = 0;
                break;
        }
    }
}

/**
 * @brief Fonction pour afficher le menu admin
 * 
 */
int menu_admin(VilleIUT *tiut[], Candidat *tcand[], User *tUsers[], int typeUtilisateur, int nbVilles, int nbcand, int nbUsers, int *OuvrirCandidatures, int nbrPlaces)
{
    int choix=0;
    char departement[30], ville[30], responsable[30];

    while(choix != 11)
    {
        afficherMenu(typeUtilisateur);
        choix = choix_menu_admin();

        switch(choix)
        {
            case 1:
            afficherVille(tiut, nbVilles); // Affiche toutes les villes
            break;
            
            case 2:
                afficherDeptIUT(tiut,nbVilles); // Affiche les départements de chaque IUT
                break;
                
            case 3:
                printf("Saisir la ville : ");
                scanf("%s",ville);
                printf("Saisir le département : ");
                scanf("%s",departement);
                afficherNbPlaces(tiut,nbVilles,ville,departement); 
                break;
                
            case 4:
                printf("\nsaisir département : \n\n");
                scanf("%s",departement);
                printf("\n");
                affichageVilleIUT(tiut,nbVilles,departement); // Affiche les IUT d'un département bien précis
                break;

            case 5:
                printf("Saississez ville : ");
                scanf("%s",ville);
                printf("Saisissez departement : ");
                scanf("%s",departement);
                printf("Saisissez nombre de places : ");
                scanf("%d", &nbrPlaces);
                modifNbPlaces(tiut,nbVilles,ville,departement,nbrPlaces); // Modifie le nombre de places d'un IUT
                break;

            case 6:
                printf("Saississez ville : ");
                scanf("%s",ville);
                printf("Saisissez departement : ");
                scanf("%s",departement);
                printf("Saisissez nombre de places : ");
                scanf("%d", &nbrPlaces);
                printf("Saisissez responsable : ");
                scanf("%s",responsable);
                nouveauDept(tiut,nbVilles,ville,departement,nbrPlaces,responsable); // Ajoute un nouveau département
                break;

            case 7:
                printf("Saississez ville : ");
                scanf("%s",ville);
                printf("Saisissez le departement à supprimer: ");
                scanf("%s",departement);
                supprDept(tiut,nbVilles,ville,departement); // Supprime un département
                break;
            
            case 8: // Afficher les candidatures
                affichageCandidature(tcand, nbcand);
                break;

            case 9:
                printf("Que voulez vous faire ?\n");
                printf("1) Lancer phase candidature\n");
                printf("2) Arrêter phase candidature\n");
                scanf("%d", OuvrirCandidatures);
                while(*OuvrirCandidatures != 1 && *OuvrirCandidatures != 2)
                {
                    printf("Erreur, veuillez saisir 1 ou 2\n");
                    scanf("%d", OuvrirCandidatures);
                }
                if(*OuvrirCandidatures == 1)
                    printf("La phase de candidature est ouverte\n");
                    
                else if(*OuvrirCandidatures == 2)
                    printf("La phase de candidature est fermée\n");
                    
                break;
                

            case 11: // Exit
                return typeUtilisateur = 0;
                break;
        }
        
    }
}
