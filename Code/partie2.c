/**
 * @file partie2.c
 * @author Mathéo Hersan, Yvan Calatayud, Leo Tuaillon
 * @brief Ensemble des fonctions de la partie 2 du projet SAE
 * @date 2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */


#include"sae2.h"

/**
 * @brief Fonction qui permet de créer un nouveau candidat
 *
 * @author Leo Tuaillon / Yvan Calatayud / Hersan Mathéo
 * @param tcand tableau de candidat
 * @param nb nombre de candidat
 * @param nom nom du candidat
 * @param prenom prenom du candidat
 * @param notes tableau de notes
 */

Candidat **nouveauCandidat(Candidat *tcand[], int* nb, int *max, char nom[], char prenom[], float notes[]) 
{
    int trouve, pos, i;
    int num = tcand[*nb - 1]->num + 1;
    float note1, note2, note3, note4;
    Candidat **aux, *c;

    pos = rechercheDicoCandidature(tcand, *nb, num, &trouve);
    if (trouve == 0)
    {
        if(*nb == *max)
        {
            *max = *max + 5;
            aux = (Candidat**) realloc(tcand, *max * sizeof(Candidat*));
            if(aux == NULL)
            {
                printf("Erreur realloc\n");
                exit(1);
            }
            tcand = aux;
        }
        for(i = *nb; i > pos; i--)
            tcand[i] = tcand[i-1];
        
        c = (Candidat*) malloc(sizeof(Candidat));
        if(c == NULL)
        {
            printf("Erreur malloc\n");
            exit(1);
        }
        c->num = num;
        strcpy(c->nom, nom);
        strcpy(c->prenom, prenom);
        printf("Entrez vos notes en : \n");
        printf("Maths : ");
        scanf("%f", &note1);
        printf("Français : ");
        scanf("%f", &note2);
        printf("Anglais : ");
        scanf("%f", &note3);
        printf("Specialité : ");
        scanf("%f", &note4);

        c->notes[0] = note1;
        c->notes[1] = note2;
        c->notes[2] = note3;
        c->notes[3] = note4;
        
        tcand[pos] = c;
        *nb = *nb + 1;

        return tcand;
    }
    else
    {
        printf("Le candidat existe deja\n");
        return tcand;
    }

}

/**
 * @brief Fonction qui permet de créer une liste de candidature vide
 *
 * @author Yvan Calatayud
 * @return ListeCandidature liste de candidature vide
 */
ListeCandidature listeVideCandidature(void)
{
    return NULL;
}

/**
 * @brief Fonction qui permet d'insérer une candidature en tête de liste de candidature et retourne la liste de candidature
 *
 * @author Hersan Mathéo
 * @param c liste de candidature
 * @param ville ville de la candidature
 * @param departement departement de la candidature
 * @param decisionDept décision du département
 * @param decision décision finale
 */
ListeCandidature insererChoixEnTete(ListeCandidature c, char ville[],char departement[], int decisionDept, int decision)
{
    MaillonCandidature *m;
    m = (MaillonCandidature *)malloc(sizeof(MaillonCandidature));
    if (m == NULL)
    {
        printf("Erreur malloc\n");
        exit(1);
    }
    strcpy(m->ville, ville);
    strcpy(m->departement, departement);
    m->decisionDept = decisionDept;
    m->decision = decision;
    m->suiv = c;
    return m;
}

/**
 * @brief Fonction qui permet d'insérer une candidature dans une liste de candidature en ordre croissant de ville et retourne la liste de candidature triée par ville croissante
 * 
 * @author Yvan Calatayud
 * @param c liste de candidature
 * @param ville ville de la candidature
 * @param departement departement de la candidature
 * @param decisionDept décision du département
 * @param decision décision finale
 * @return ListeCandidature liste de candidature triée par ville croissante
 */
ListeCandidature insertionChoixCroissante(ListeCandidature c, char ville[],char departement[], int decisionDept, int decision)
{
    if (c == NULL)
        return insererChoixEnTete(c, ville, departement, decisionDept, decision);
    if (strcmp(ville,c-> ville) < 0)
        return insererChoixEnTete(c, ville, departement, decisionDept, decision);
    if (strcmp(ville, c->ville) == 0)
        return c;
    c -> suiv = insertionChoixCroissante(c->suiv, ville, departement, decisionDept, decision);
    return c;
}

/**
 * @brief Fonction qui permet d'afficher une liste de candidature
 *
 * @author Hersan Mathéo
 * @param lc liste de candidature
 */
void afficherlisteCandidature(ListeCandidature lc)
{
    if (lc == NULL)
    {
        printf("\n");
        return;
    }
    printf("Ville: %s\n", lc->ville);
    printf("Departement: %s\n", lc->departement);
    printf("Decision de departement: %d\n", lc->decisionDept);
    printf("Decision finale: %d\n", lc->decision);
    afficherlisteCandidature(lc->suiv);
}

/**
 * @brief Fonction qui permet de rechercher de manière dicotomique un candidat dans un tableau de candidat trié par numéro de candidat
 *
 * @author Hersan Mathéo
 * @param cand tableau de candidat
 * @param nb nombre de candidat
 * @param num numéro de candidat
 * @param trouve pointeur sur un entier qui permet de savoir si le candidat a été trouvé ou non
 */
int rechercheDicoCandidature(Candidat *cand[],int nb, int num, int *trouve)
{
    int inf=0,sup=nb-1,mil;
    while(inf<=sup)
    {
        mil = (inf+sup)/2;
        if(num == cand[mil]->num)
        {
            *trouve = 1;
            return mil;
        }
        if(num < cand[mil]->num)
            sup = mil-1;
        else
            inf = mil+1;
    }
    *trouve = 0;
    return inf;
}

/**
 * @brief Fonction qui permet de savoir si un candidat appartient à une liste de candidature ou non
 *
 * @author Yvan Calatayud
 * @param c liste de candidature
 * @param ville ville de la candidature
 * @param departement departement de la candidature
 * @param decisionDept décision du département
 * @param decision décision finale
 * @return 1 si le candidat appartient à la liste de candidature, 0 sinon
 */
int appartientChoix(ListeCandidature c, char ville[],char departement[], int decisionDept, int decision)
{
    if (c == NULL)
        return 0;
    if (strcmp(departement, c->departement) < 0)
        return 0;
    if (strcmp(departement, c->departement) == 0)
        return 1;
    return appartientChoix(c->suiv, ville, departement, decisionDept, decision);
}

/**
 * @brief Fonction qui permet de savoir si une ville appartient à une liste de candidature ou non
 *
 * @author Yvan Calatayud
 * @param c liste de candidature
 * @param ville ville de la candidature
 * @return 1 si la ville appartient à la liste de candidature, 0 sinon
 */
int appartientVille(ListeCandidature c, char ville[])
{
    if (c == NULL)
        return 0;
    if (strcmp(ville, c->ville) < 0)
        return 0;
    if (strcmp(ville, c->ville) == 0)
        return 1;
    return appartientVille(c->suiv, ville);
}

/**
 * @brief Fonction qui permet d'insérer un candidat dans un tableau de candidat trié par numéro de candidat
 *
 * @author Hersan Mathéo
 * @param cand tableau de candidat
 * @param nb nombre de candidat
 * @param num numéro de candidat
 * @param nom nom du candidat
 * @param prenom prénom du candidat
 * @param notes tableau de notes
 * @param ville ville de la candidature
 * @param departement departement de la candidature
 * @param decisionDept décision du département
 * @param decision décision finale
 */
void nouveauChoix(Candidat *cand[],int nb, int num, char nom[], char prenom[], float notes[],char ville[],char departement[], int decisionDept, int decision)
{
    int trouve, pos;
    pos = rechercheDicoCandidature(cand,nb,num,&trouve);
    if(trouve == 0)
    {
        printf("Candidat inconnue\n");
        return;
    }
    
    if(appartientVille(cand[pos]->lCandidature,ville))
    {
        printf("Ville deja existante\n");
        return;
    }
        
    
    cand[pos]->lCandidature = insertionChoixCroissante(cand[pos]->lCandidature,ville,departement,decisionDept,decision);
}

/**
 * @brief Fonction qui permet de charger un fichier de candidat et de le mettre dans un tableau de candidat trié par numéro de candidat
 *
 * @author Yvan Calatayud / Hersan Mathéo / Leo Tuaillon
 * @param nomFic nom du fichier
 * @param nbcand nombre de candidat
 * @param max nombre maximum de candidat
 * @return tableau de candidat
 */
Candidat** chargementTest(char nomFic[], int *nbcand,int *max)
{
    int trouve, pos,i,decisionDept,decision,num,nbChoix,nbTotal;
    char nom[30],prenom[30],ville[30],departement[30];
    float notes[4],note1,note2,note3,note4;
    FILE *f;
    f = fopen(nomFic, "r");
    if(f == NULL)
    {
        printf("Erreur d'ouverture du fichier\n");
        exit(1);
    }
    *nbcand = 0;
    *max =5;

    Candidat **tcand ,**aux;
    tcand = (Candidat**)malloc(sizeof(Candidat*));
    if(tcand == NULL)
    {
        printf("Erreur d'allocation mémoire\n");
        exit(1);
    }
    fscanf(f,"%d",&nbTotal);
    fscanf(f,"%d%*c",&num);

    while(nbTotal>0)
    {
        pos = rechercheDicoCandidature(tcand,*nbcand,num,&trouve);
        if(trouve == 0)
        {
            if(*nbcand == *max)
            {
                *max = *max + 5;
                aux = (Candidat **)realloc(tcand,*max*sizeof(Candidat*));
                if(aux==NULL)
                {
                    printf("Erreur de réallocation mémoire\n");
                    exit(1);
                }
                tcand = aux;
            }
            
            tcand[pos] = (Candidat*) malloc(sizeof(Candidat));
            if(tcand[pos]==NULL)
            {
                printf("Erreur d'allocation mémoire\n");
                exit(1);
            }
            tcand[pos]->num = num;
            tcand[pos]->lCandidature = listeVideCandidature();
            *nbcand = *nbcand+1;
        }
        // utiliser fscanf pour lire les noms et les prénoms
        fscanf(f, "%s%*c",nom);
        fgets(prenom, 30, f);
        prenom[strlen(prenom)-1] = '\0';
        fscanf(f,"%f %f %f %f",&note1,&note2,&note3,&note4);
        
        notes[0] = note1;
        notes[1] = note2;
        notes[2] = note3;
        notes[3] = note4;

        strcpy(tcand[pos]->nom,nom);
        strcpy(tcand[pos]->prenom,prenom);
        tcand[pos]->notes[0] = notes[0];
        tcand[pos]->notes[1] = notes[1];
        tcand[pos]->notes[2] = notes[2];
        tcand[pos]->notes[3] = notes[3];
        fscanf(f,"%d",&nbChoix);
        for(i=0;i<nbChoix;i++)
        {
            fscanf(f,"%s%*c",ville);
            fscanf(f,"%s%*c",departement);
            fscanf(f,"%d%*c",&decisionDept);
            fscanf(f,"%d%*c",&decision);
            nouveauChoix(tcand,*nbcand,num,nom,prenom,notes,ville,departement,decisionDept,decision);
        }
        fscanf(f,"%d%*c",&num);
        nbTotal = nbTotal - 1;
    }
    fclose(f);
    return tcand;
}

/**
 * @brief Fonction qui permet d'afficher les candidats d'un tableau de candidat
 *
 * @author Yvan Calatayud / Hersan Mathéo 
 * @param cand tableau de candidat
 * @param nb nombre de candidat
 */
void affichageCandidature(Candidat *cand[],int nb)
{
    int i;
    for(i=0;i<nb;i++)
    {
        printf("Candidat n°%d :\n\n\tNom : %s\n\tPrenom : %s\n",cand[i]->num,cand[i]->nom,cand[i]->prenom);
        printf("\tNotes :\n\t\tMaths : %.2f\n\t\tFrançais : %.2f\n\t\tAnglais : %.2f\n\t\tSpecialité : %.2f\n\n",cand[i]->notes[0],cand[i]->notes[1],cand[i]->notes[2],cand[i]->notes[3]);
        printf("Choix :\n");
        affichageChoix(cand[i]->lCandidature);
    }
}

/**
 * @brief Fonction qui permet d'afficher les choix
 *
 * @author Yvan Calatayud / Hersan Mathéo 
 * @param c liste de candidature
 */
void affichageChoix(ListeCandidature c)
{
    if(c == NULL)
        return;
    printf("\tVille : %s\n", c->ville);
    printf("\t\tDepartement : %s\n", c->departement);
    printf("\t\tDecision departement : %d\n", c->decisionDept);
    printf("\t\tDecision : %d\n\n", c->decision);
    affichageChoix(c->suiv);
}


/**
 * @brief  Fonction qui permet de supprimer en tete de liste de candidature
 *
 * @author Yvan Calatayud / Hersan Mathéo / Leo Tuaillon 
 * @param l liste de candidature
 * @return liste de candidature
 */
ListeCandidature supprimerEnTeteCand(ListeCandidature l)
{
    MaillonCandidature *aux;
    if (l == NULL)
    {
        printf("Erreur de suppression en tete");
        exit(1);
    }
    aux = l;
    l = l->suiv;
    free(aux);
    return l;
}

/**
 * @brief Fonction qui permet de supprimer un candidat et ses choix 
 *
 * @author Yvan Calatayud / Hersan Mathéo / Leo Tuaillon
 * @param cand tableau de candidat
 * @param nb nombre de candidat
 */
void supprimerCandidatCand(Candidat *cand[],int *nb)
{
    int num,trouve,pos,i;
    printf("Entrez le numero de la Candidat a supprimer : ");
    scanf("%d",&num);
    pos = rechercheDicoCandidature(cand,*nb,num,&trouve);
    if(trouve == 0)
    {
        printf("Candidature inconnue\n");
        return;
    }
    
    while(cand[pos]->lCandidature != NULL)
    {
        cand[pos]->lCandidature = supprimerEnTeteCand(cand[pos]->lCandidature);
    }

    for(i=pos;i<*nb;i++)
    {
        cand[i] = cand[i+1];
    }
    *nb = *nb - 1;
}

/**
 * @brief Fonction qui permet d'afficher une candidature
 *
 * @author Yvan Calatayud / Hersan Mathéo / Leo Tuaillon
 * @param tcand tableau de candidat
 * @param nb nombre de candidat
 * @param num numero de la candidature
 */
void affichage1Candidature(Candidat *tcand[],int nb, int num)
{
    int trouve, pos;
    pos = rechercheDicoCandidature(tcand,nb,num,&trouve);
    if(trouve == 0)
    {
        printf("Candidature inconnue\n");
        return;
    }
    printf("Numero de Candidat: %d\n", tcand[pos]->num);
    printf("Nom: %s\n", tcand[pos]->nom);
    printf("Prenom: %s\n", tcand[pos]->prenom);
    printf("Notes: Math = %f, Francais = %f, Anglais = %f, Specialité = %f\n", tcand[pos]->notes[0],tcand[pos]->notes[1],tcand[pos]->notes[2],tcand[pos]->notes[3]);
    afficherlisteCandidature(tcand[pos]->lCandidature);
}

/**
 * @brief Fonction qui permet de sauvegarder les candidats dans un fichier texte candidatures.txt
 *
 * @author Yvan Calatayud / Hersan Mathéo / Leo tuaillon 
 * @param tcand tableau de candidat
 * @param nbCandidat nombre de candidat
 */
void sauvegardeCandidats(Candidat *tcand[], int nbCandidat)
{
    FILE *f;
    int i;
    f = fopen("candidatures.txt","w");
    if(f == NULL)
    {
        printf("Erreur d'ouverture du fichier");
        exit(1);
    }
    fprintf(f,"%d\n",nbCandidat);
    for(i=0;i<nbCandidat;i++)
    { 
        fprintf(f,"%d\n",tcand[i]->num);
        fprintf(f,"%s\n",tcand[i]->nom);
        fprintf(f,"%s\n",tcand[i]->prenom);
        fprintf(f,"%.2f\t%.2f\t%.2f\t%.2f\n",tcand[i]->notes[0],tcand[i]->notes[1],tcand[i]->notes[2],tcand[i]->notes[3]);
        fprintf(f,"%d\n",longueurCandidature(tcand[i]->lCandidature));
        while(tcand[i]->lCandidature != NULL)
        {
            

            sauvegardeCandidatures(tcand[i]->lCandidature,f);
            tcand[i]->lCandidature = tcand[i]->lCandidature->suiv;
            
            
        }

    }
    fclose(f);
}

/**
 * @brief Fonction qui permet de sauvegarder les candidatures dans un fichier texte candidatures.txt
 *
 * @author Yvan Calatayud / Hersan Mathéo / Leo tuaillon
 * @param l liste de candidature
 * @param f fichier texte
 */
void sauvegardeCandidatures(ListeCandidature l, FILE *f)
{
    if(l == NULL)
        return;
    fprintf(f,"%s\n",l->ville);
    fprintf(f,"%s\n",l->departement);
    fprintf(f,"%d\n",l->decisionDept);
    fprintf(f,"%d\n",l->decision);
}

/**
 * @brief Fonction récursive qui permet de calculer la longueur d'une liste de candidature
 *
 * @author Leo tuaillon
 * @date 2022
 * @param l liste de candidature
 * @return la longueur de la liste
 */
int longueurCandidature(ListeCandidature l)
{   
    if(l == NULL)
        return 0;
    return 1+longueurCandidature(l->suiv);
}

/**
 * @brief Fonction qui permet de supprimer une candidature précise
 *
 * @author Yvan Calatayud / Leo tuaillon 
 * @param tcand tableau de candidat
 * @param nbCandidat nombre de candidat
 */
void supprimerCandidaturePrecise(Candidat *cand[],int *nb)
{
    int num,trouve,pos;
    char ville[50],departement[50];
    printf("Entrez le numero de la Candidat : ");
    scanf("%d",&num);
    pos = rechercheDicoCandidature(cand,*nb,num,&trouve);
    if(trouve == 0)
    {
        printf("Candidature inconnue\n");
        return;
    }
    printf("Entrez la ville : ");
    scanf("%s",ville);
    printf("Entrez le departement : ");
    scanf("%s",departement);
    cand[pos]->lCandidature = supprimerCandidature(cand[pos]->lCandidature,ville,departement);
}

/**
 * @brief Fonction récursive qui permet de supprimer une candidature précise
 * 
 * @author Yvan Calatayud / Leo tuaillon 
 * @param l liste de candidature
 * @param ville ville de la candidature
 * @param departement departement de la candidature
 * @return la liste de candidature sans la candidature supprimée
 */
ListeCandidature supprimerCandidature(ListeCandidature l, char *ville, char *departement)
{
    if(l == NULL)
        return NULL;
    if(strcmp(l->ville,ville) == 0 && strcmp(l->departement,departement) == 0)
    {
        ListeCandidature tmp = l;
        l = l->suiv;
        free(tmp);
        return l;
    }
    l->suiv = supprimerCandidature(l->suiv,ville,departement);
    return l;
}
